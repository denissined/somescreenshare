#include <iostream>
#include <string>
#include <tchar.h>
#include <vector>
#include <time.h>
#include <sstream>
#include <windows.h>
#include <windowsx.h>
#include <shellapi.h>
#include <wininet.h>
#include "wtypes.h"
#include <gdiplus.h>
#include <Gdiplusimaging.h> 
//#include "main.h"
#include <commctrl.h >

#pragma comment(lib,"comctl32.lib")

#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#pragma comment (lib,"Gdiplus.lib")
#pragma comment(lib, "Wininet")
#define GET_X_LPARAM(lp) ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp) ((int)(short)HIWORD(lp))

using namespace Gdiplus;

std::string fullPathToFile;
std::string urlForServer;
const wchar_t* someNewFileName;

int click_counter = 0;
int start_capture = 0;
bool lButtonWasDown = false;

POINT first_point, second_point;
POINT first_point_draw, second_point_draw;
POINT p_mouse;

HWND windowHandle = NULL;
HWND hArrow = NULL;
HWND hCopyToBuffer = NULL;
HWND hUpload = NULL;
HWND hRect = NULL;
HWND hSave = NULL;
HWND hScreenshot = NULL;
HWND textBoxSome = NULL;
HWND bitmapBOX = NULL;
HBITMAP someScrImage = NULL;
static HFONT s_hFont = NULL;

#define ID_BUTTON 1

using std::runtime_error;




static std::string utf16ToUTF8(const std::wstring& s)
{
	const int size = ::WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, NULL, 0, 0, NULL);

	std::vector<char> buf(size);
	::WideCharToMultiByte(CP_UTF8, 0, s.c_str(), -1, &buf[0], size, 0, NULL);

	return std::string(&buf[0]);
}

char* randomGet(int num)
{
	srand((int)time(NULL));
	static const char ab[] = "0123456789ABCDEFGHIGKLMNOPQRSTUVWXYZ"; //Alphabet&Digit
	char* targ = new char[num + 1];
	for (int i = 0; i < num; ++i) {
		targ[i] = ab[rand() % 36];
	}
	targ[num] = '\0';

	return targ;
}

std::string ExePath() {
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	return std::string(buffer).substr(0, pos);
}


std::string getCurDateString()
{
	time_t rawtime;
	struct tm timeinfo;
	char buffer[80];

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	strftime(buffer, 80, "%d-%m-%Y", &timeinfo);
	std::string str_date(buffer);

	return str_date;
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

wchar_t* getNewFileName()
{

	int count_num = 0;

	char* randomStringVal = randomGet(20);

	static const char some[] = ".png"; //Alphabet&Digit

	char* kek = new char[strlen(randomStringVal) + strlen(some) + 1];

	for (int i = 0; i < (int)strlen(randomStringVal); ++i)
	{
		kek[i] = randomStringVal[i];
		count_num = i;
	}

	for (int i = count_num - 1, k = 0; i < (int)count_num + (int)strlen(some); ++i, ++k)
	{
		kek[i] = some[k];
	}

	size_t size = strlen(kek) + 1;

	wchar_t* fileName = new wchar_t[size];

	const wchar_t* someFileName = fileName;

	size_t outSize;
	mbstowcs_s(&outSize, fileName, size, kek, size - 1);

	return  fileName;
}

void SaveImageFile(HBITMAP hBmp, wchar_t* fileName)
{
	std::string dateFolder(getCurDateString());

	std::wstring ws(fileName);
	std::string outFileName(ws.begin(), ws.end());

	urlForServer = "\\" + dateFolder + "\\" + outFileName.c_str();

	outFileName = ExePath() + "\\" + dateFolder + "\\" + outFileName.c_str();

	std::string OutputFolder = ExePath() + "\\" + dateFolder;

	fullPathToFile = OutputFolder;


	if (CreateDirectory(OutputFolder.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		MessageBox(NULL, TEXT(OutputFolder.c_str()),
			TEXT("Error!"), MB_ICONERROR);
		// CopyFile(...)
	}
	else
	{
		MessageBox(NULL, TEXT("Failed to Create dir"),
			TEXT("Error!"), MB_ICONERROR);
		// Failed to create directory.
	}

	Gdiplus::Bitmap newBitmap(hBmp, NULL);

	std::wstring widestr = std::wstring(outFileName.begin(), outFileName.end());
	const wchar_t* widecstr = widestr.c_str();

	// Save bitmap (as a png)
	CLSID pngClsid;
	int result = GetEncoderClsid(L"image/png", &pngClsid);
	if (result == -1)
		throw runtime_error("GetEncoderClsid");
	if (Ok != newBitmap.Save(widecstr, &pngClsid, NULL))
		throw runtime_error("Bitmap::Save");
}

//Save edited Screenshot
void saveNewScreenshot()
{
	RECT  rcCli;
	GetClientRect(WindowFromDC((HDC)GetDC(bitmapBOX)), &rcCli);

	// copy screen to bitmap
	HDC     hScreen = GetDC(bitmapBOX);
	HDC     hDC = CreateCompatibleDC(hScreen);
	HBITMAP hBitmap = CreateCompatibleBitmap(hScreen, abs(rcCli.right - rcCli.left), abs(rcCli.bottom - rcCli.top));
	HGDIOBJ old_obj = SelectObject(hDC, hBitmap);
	BOOL    bRet = BitBlt(hDC, 0, 0, abs(rcCli.right - rcCli.left), abs(rcCli.bottom - rcCli.top), hScreen, rcCli.left, rcCli.top, SRCCOPY);

	Gdiplus::Bitmap newImage(hBitmap, NULL);

	wchar_t* fileName = getNewFileName();

	const wchar_t* someFileName = fileName;

	someNewFileName = someFileName;

	SaveImageFile(hBitmap, fileName);

	SetWindowTextA(
		textBoxSome,
		(LPCSTR)"Image Saved!"
	);
}

void SetFont(HWND someHWND)
{
	LOGFONT logical;
	logical.lfHeight = 16;
	logical.lfWidth = 0;
	logical.lfEscapement = 0;
	logical.lfOrientation = 0;
	logical.lfWeight = 400;
	logical.lfItalic = FALSE;
	logical.lfUnderline = FALSE;
	logical.lfStrikeOut = FALSE;
	logical.lfCharSet = DEFAULT_CHARSET;
	logical.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logical.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logical.lfQuality = DEFAULT_QUALITY;
	logical.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	strcpy_s(logical.lfFaceName, "Franklin Gothic Medium");
	s_hFont = CreateFontIndirect(&logical);
	BOOL fRedraw = TRUE;
	SendMessage(someHWND, WM_SETFONT, (WPARAM)s_hFont, MAKELPARAM(fRedraw, 0));
}

// Get the horizontal and vertical screen sizes in pixel
void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}

int upload(LPCSTR fileName, LPCSTR fileNameHosing)
{
	HINTERNET hInternet = InternetOpen(NULL, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
	HINTERNET hFtpSession = InternetConnect(hInternet, "s12ua.netxi.host", INTERNET_DEFAULT_FTP_PORT, "share@share.shardanov.com", "W^es9A#A=DN", INTERNET_SERVICE_FTP, INTERNET_FLAG_PASSIVE, 0);

	FtpCreateDirectoryA(hFtpSession, (LPCSTR)getCurDateString().c_str());
	FtpSetCurrentDirectoryA(hFtpSession, getCurDateString().c_str());

	FtpPutFile(hFtpSession, fileName, fileNameHosing, FTP_TRANSFER_TYPE_BINARY, 0);

	//	std::string s = 
							//	"BOX X: " + std::to_string(BOXPOS.x) + " BOX Y: " + std::to_string(BOXPOS.y)+
							//	" X1: "+std::to_string(first_point_draw.x)+" Y1: "+ std::to_string(first_point_draw.y)
							//	+ " - X2: " + std::to_string(second_point_draw.x) + " Y2: " + std::to_string(second_point_draw.y);


	SetWindowTextA(
		textBoxSome,
		(LPCSTR)"File Uploaded"
	);

	InternetCloseHandle(hFtpSession);
	InternetCloseHandle(hInternet);
	return 0;
}

void showScreenshot(HDC hDC, HBITMAP someScr)
{
	BITMAP bm;
	::GetObject(someScr, sizeof(bm), &bm);

	if (SetWindowPos(bitmapBOX, 0, 0, 0, bm.bmWidth, bm.bmHeight, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER))
	{

		Gdiplus::Graphics g((HDC)GetDC(bitmapBOX));
		Gdiplus::Bitmap newBitmapScreenShot(someScr, NULL);
		g.DrawImage(&newBitmapScreenShot, 0, 0);
		//InvalidateRect(bitmapBOX, bm.bmWidth, bm.bmHeight);
	}
}

void screenshot(POINT a, POINT b)
{
	// copy screen to bitmap
	HDC     hScreen = GetDC(NULL);
	HDC     hDC = CreateCompatibleDC(hScreen);
	HBITMAP hBitmap = CreateCompatibleBitmap(hScreen, abs(b.x - a.x), abs(b.y - a.y));
	HGDIOBJ old_obj = SelectObject(hDC, hBitmap);
	BOOL    bRet = BitBlt(hDC, 0, 0, abs(b.x - a.x), abs(b.y - a.y), hScreen, a.x, a.y, SRCCOPY);

	// save bitmap to clipboard
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hBitmap);
	CloseClipboard();

	someScrImage = hBitmap;

	// clean up
	SelectObject(hDC, old_obj);
	DeleteDC(hDC);
	ReleaseDC(NULL, hScreen);
	DeleteObject(hBitmap);
}

VOID DrawSomePosition(HDC hdc, POINT pPointsSome)
{
	Gdiplus::Graphics graphics(hdc);

	// Create a Pen object.
	Pen somePen(Color(255, 15, 0, 255), 3);

	// Create an array of Point objects that define the polygon.
	Point point1(pPointsSome.x, pPointsSome.y + 5);
	Point point2(pPointsSome.x + 5, pPointsSome.y);
	Point point3(pPointsSome.x, pPointsSome.y + 5);
	Point point4(pPointsSome.x + 5, pPointsSome.y);
	Point points[4] = { point1, point2, point3, point4 };
	Point* pPoints = points;

	// Draw the polygon.
	graphics.DrawPolygon(&somePen, pPoints, 4);
}

//Some Global values
bool post_arrow = false;
int first_point_arrow_x = 0, first_point_arrow_y = 0;
int second_point_arrow_x = 0, second_point_arrow_y = 0;
int point_num = 0;
bool arrowPostStatus = false;
bool secondStepDraw = false;
POINT BOXPOS;

//NEED TO FIX ARROW REDRAW BECAUSE ARROW DISSAPEAR AFTER UPLOAD IMAGE!
void drawArrow()
{
	Gdiplus::Graphics g((HDC)GetDC(bitmapBOX));

	Pen pen(Color(255, 0, 0, 255), 8);
	Status buff;

	//buff = pen.SetStartCap(LineCapArrowAnchor);
	buff = pen.SetEndCap(LineCapArrowAnchor);
	//NEED TO FIX DRAW LINE
	//buff = g.DrawLine(&pen, first_point_draw.x - BOXPOS.x, first_point_draw.y - BOXPOS.y, second_point_draw.x - BOXPOS.x, second_point_draw.y - BOXPOS.y);

	//newImage.Dispose();
}

//Set New Font for Elements
void setNewFontForElements()
{
	SetFont(hArrow);
	SetFont(hRect);
	SetFont(hSave);
	SetFont(hScreenshot);
	SetFont(textBoxSome);
	SetFont(hUpload);
	SetFont(hCopyToBuffer);
}

void disableElement(HWND someElement)
{
	EnableWindow(someElement,
		FALSE
	);
}

void enableElement(HWND someElement)
{
	EnableWindow(someElement,
		TRUE
	);
}

//Here we get window messages 
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	HDC hdc;
	PAINTSTRUCT ps;

	switch (message)
	{
	case WM_CREATE:

		return 0;
		break;

	case WM_ACTIVATE:
		//InvalidateRect(bitmapBOX, NULL, TRUE);
		//UpdateWindow(bitmapBOX);
		//SendMessage(hwnd, WM_PAINT, 0, 0);
		return 0;
		break;

	case WM_SIZE:
		//InvalidateRect(bitmapBOX, NULL, TRUE);
		//UpdateWindow(bitmapBOX);
		//SendMessage(hwnd, WM_PAINT, 0, 0);
		return 0;
		break;

	case WM_PAINT:
		setNewFontForElements();

		hdc = BeginPaint(hwnd, &ps);

		InvalidateRect(bitmapBOX, NULL, TRUE);
		UpdateWindow(bitmapBOX);

		// Show Screenshot and draw emenets
		showScreenshot(NULL, someScrImage);
		if (post_arrow)
		{
			drawArrow();
			post_arrow = false;
		}

		EndPaint(hwnd, &ps);
		return 0;
		break;

	case WM_LBUTTONDOWN:
		if (wparam == MK_LBUTTON)
		{
			if (arrowPostStatus)
			{
				point_num++;

				if (point_num == 1)
				{
					if (secondStepDraw == false)
					{
						first_point_draw.x = GET_X_LPARAM(lparam);
						first_point_draw.y = GET_Y_LPARAM(lparam);

						secondStepDraw = true;
					}
				}
				else if (point_num == 2)
				{
					SetWindowTextA(
						textBoxSome,
						(LPCSTR)"Select Second Point."
					);

					second_point_draw.x = GET_X_LPARAM(lparam);
					second_point_draw.y = GET_Y_LPARAM(lparam);

					point_num = 0;
					post_arrow = true;
					arrowPostStatus = false;
					secondStepDraw = false;

					RECT rect = { NULL };
					if (GetWindowRect(bitmapBOX, &rect)) {
						BOXPOS.x = rect.left;
						BOXPOS.y = rect.top;
					}

					ScreenToClient(hwnd, &BOXPOS);


					//	std::string s = 
						//	"BOX X: " + std::to_string(BOXPOS.x) + " BOX Y: " + std::to_string(BOXPOS.y)+
						//	" X1: "+std::to_string(first_point_draw.x)+" Y1: "+ std::to_string(first_point_draw.y)
						//	+ " - X2: " + std::to_string(second_point_draw.x) + " Y2: " + std::to_string(second_point_draw.y);


					//	SetWindowTextA(
						//	textBoxSome,
						//	(LPCSTR)s.c_str()
						//);

					SendMessage(hwnd, WM_PAINT, 0, 0);
				}
			}
		}
		return 0;
		break;

	case WM_KEYDOWN:
		if (wparam == 'S')
		{
			if (GetCursorPos(&p_mouse))
			{
				if (click_counter == 0)
				{
					first_point.x = p_mouse.x;
					first_point.y = p_mouse.y;

					DrawSomePosition(GetDC(NULL), first_point);

					click_counter = 1;
					lButtonWasDown = false;
				}

				else
				{
					second_point.x = p_mouse.x;
					second_point.y = p_mouse.y;

					screenshot(first_point, second_point);

					click_counter = 0;

					first_point.x = 0;
					second_point.x = 0;
					second_point.x = 0;
					second_point.y = 0;
					start_capture = 0;
					lButtonWasDown = false;

					InvalidateRect(bitmapBOX, NULL, TRUE);
					UpdateWindow(bitmapBOX);
					SendMessage(hwnd, WM_PAINT, 0, 0);
				}


			}

		}
		if (wparam == VK_ESCAPE)
		{
			PostQuitMessage(0);

		}
		return 0;
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;

	case WM_COMMAND:
		if (LOWORD(wparam))
		{
	case BN_CLICKED:
		// see which button was clicked
		if ((HWND)lparam == hRect)
		{



		}
		if ((HWND)lparam == hArrow)
		{
			if (point_num == 0)
			{
				arrowPostStatus = true;
			}

			//MessageBox(NULL, TEXT("KEK2!"),
				//TEXT("Error!"), MB_ICONERROR);
		}
		if ((HWND)lparam == hScreenshot)
		{
			//if (point_num == 0)
			//{
			//	arrowPostStatus = true;
			//}

			//MessageBox(NULL, TEXT("KEK2!"),
				//TEXT("Error!"), MB_ICONERROR);
		}
		if ((HWND)lparam == hSave)
		{
			saveNewScreenshot();
		}
		if ((HWND)lparam == hUpload)
		{
			std::string utf8String = utf16ToUTF8(someNewFileName);

			fullPathToFile = std::string(fullPathToFile + "\\" + utf8String);

			upload(fullPathToFile.c_str(), utf8String.c_str());

			system(std::string("start https://share.shardanov.com/screenshots" + urlForServer).c_str());

		}
		if ((HWND)lparam == hCopyToBuffer)
		{

		}
		break;

		}
		return 0;
		break;

	default:
		return DefWindowProc(hwnd, message, wparam, lparam);
	}
	return 0;
}

void HideConsole()
{
	::ShowWindow(::GetConsoleWindow(), SW_HIDE);
}

void ShowConsole()
{
	::ShowWindow(::GetConsoleWindow(), SW_SHOW);
}

bool IsConsoleVisible()
{
	return ::IsWindowVisible(::GetConsoleWindow()) != FALSE;
}


int main()
{
	//ADD STYLES TO WINDOW LIKE NOT WIN 95
	INITCOMMONCONTROLSEX icc;
	icc.dwICC = ICC_ANIMATE_CLASS | ICC_NATIVEFNTCTL_CLASS | ICC_STANDARD_CLASSES;
	icc.dwSize = sizeof(icc);
	InitCommonControlsEx(&icc);

	//HIDE CONSOLE WINDOW AND FREE IT TO MAKE CREATED WINDOW WORKS
	HideConsole();
	FreeConsole();

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	if (GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) != Gdiplus::Ok)
	{
		MessageBox(NULL, TEXT("GDI+ failed to start up!"),
			TEXT("Error!"), MB_ICONERROR);

		return -1;
	}


	WNDCLASS windowClass = { 0 };
	windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hInstance = NULL;
	windowClass.lpfnWndProc = WndProc;
	windowClass.lpszClassName = "ShowMeScreen";
	windowClass.style = CS_HREDRAW | CS_VREDRAW | DS_SETFONT;
	if (!RegisterClass(&windowClass))
		MessageBox(NULL, "Could not register class", "Error", MB_OK);

	int horizontal = 0;
	int vertical = 0;

	GetDesktopResolution(horizontal, vertical);

	windowHandle = CreateWindow("ShowMeScreen",
		"ShowMeScreen",
		WS_OVERLAPPEDWINDOW,
		(horizontal - 800) / 2,
		(vertical - 600) / 2,
		800,
		600,
		NULL,
		NULL,
		NULL,
		NULL);

	ShowWindow(windowHandle, SW_SHOW);

	//Create Elements on Window: label, buttons, etc.
	hScreenshot = CreateWindow(TEXT("BUTTON"), TEXT("Make Screenshot"), WS_CHILD | WS_VISIBLE, 10, 40, 120, 20, windowHandle, (HMENU)ID_BUTTON, NULL, NULL);
	hSave = CreateWindow(TEXT("BUTTON"), TEXT("Save Screenshot"), WS_CHILD | WS_VISIBLE, 150, 40, 120, 20, windowHandle, (HMENU)ID_BUTTON, NULL, NULL);

	hArrow = CreateWindow(TEXT("BUTTON"), TEXT("Add Arrow"), WS_CHILD | WS_VISIBLE, 10, 10, 120, 20, windowHandle, (HMENU)ID_BUTTON, NULL, NULL);
	hRect = CreateWindow(TEXT("BUTTON"), TEXT("Add Rect"), WS_CHILD | WS_VISIBLE, 150, 10, 120, 20, windowHandle, (HMENU)ID_BUTTON, NULL, NULL);

	hUpload = CreateWindow(TEXT("BUTTON"), TEXT("Upload Image"), WS_CHILD | WS_VISIBLE, 290, 10, 120, 20, windowHandle, (HMENU)ID_BUTTON, NULL, NULL);
	hCopyToBuffer = CreateWindow(TEXT("BUTTON"), TEXT("Copy To Buffer"), WS_CHILD | WS_VISIBLE, 290, 40, 120, 20, windowHandle, (HMENU)ID_BUTTON, NULL, NULL);

	textBoxSome = CreateWindow(TEXT("Static"), TEXT(""), WS_VISIBLE | WS_CHILD, 10, 80, 500, 20, windowHandle, NULL, NULL, NULL);
	bitmapBOX = CreateWindow(TEXT("Static"), TEXT(""), WS_VISIBLE | WS_CHILD, 10, 120, 200, 200, windowHandle, NULL, NULL, NULL);

	if (RegisterHotKey(NULL, 1, MOD_SHIFT, 0x42))
	{
		//std::cout << "Hotkey 'SHIFT+b' registered, using MOD_NOREPEAT flag\n";
	}
	else
	{
		MessageBox(NULL, TEXT("Hotkey Failed"),
			TEXT("Error!"), MB_ICONERROR);
		//std::cout << "Error code " << GetLastError();
	}

	MSG messages;
	while (GetMessage(&messages, NULL, 0, 0) > 0)
	{
		if (messages.message == WM_HOTKEY)
		{
			//MessageBox(NULL, TEXT("Hotkey Pressed"),
			//	TEXT("Error!"), MB_ICONERROR);
			::ShowWindow(windowHandle, SW_RESTORE);
			SetForegroundWindow(FindWindowA(NULL, "ShowMeScreen"));
			SetFocus(windowHandle);
		}

		TranslateMessage(&messages);
		DispatchMessage(&messages);
	}
	DeleteObject(windowHandle); //doing it just in case
	return messages.wParam;
}