#pragma once

void SetFont(HWND someHWND)
{
	LOGFONT logical;
	logical.lfHeight = 16;
	logical.lfWidth = 0;
	logical.lfEscapement = 0;
	logical.lfOrientation = 0;
	logical.lfWeight = 400;
	logical.lfItalic = FALSE;
	logical.lfUnderline = FALSE;
	logical.lfStrikeOut = FALSE;
	logical.lfCharSet = DEFAULT_CHARSET;
	logical.lfOutPrecision = OUT_DEFAULT_PRECIS;
	logical.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	logical.lfQuality = DEFAULT_QUALITY;
	logical.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	strcpy_s(logical.lfFaceName, "Franklin Gothic Medium");
	s_hFont = CreateFontIndirect(&logical);
	BOOL fRedraw = TRUE;
	SendMessage(someHWND, WM_SETFONT, (WPARAM)s_hFont, MAKELPARAM(fRedraw, 0));
}
